path = require('path')
regenerator = require('gulp-regenerator')
gulp = require('gulp')
gutil = require('gulp-util')
ifElse = require('gulp-if-else')
minify = require('gulp-minify')
uglify = require('gulp-uglify')
streamify = require('gulp-streamify')
browserifyCss = require('browserify-css')
wrap = require('gulp-wrap')
browserify = require('browserify')
coffeeify = require('coffeeify')
csxify = require('./csxify')
watchify = require('watchify')
source = require('vinyl-source-stream')
csx = require('./gulp-csx')
del = require('del')
Emitter = require('./emitter')
_ = require('lodash')

module.exports = class Scripts extends Emitter
  constructor: (@from, @to, @appEnv, @__HACKwatch)->

  rebundle: (bundleToPath, bundleToName, appEnv)-> async =>
    startDate = new Date();
    @notify 'Bundling script ' + bundleToName
    yield new Promise (resolve, reject)=>
      @bundler.bundle()
        .on('error', reject)
        .pipe(source(bundleToName))
        .pipe(ifElse(appEnv isnt 'development', streamify.bind(@, regenerator({includeRuntime: true}))))
        .pipe(wrap('(function () { var define = undefined; <%=contents%> })();'))
        #.pipe(ifElse(appEnv isnt 'development', streamify.bind(@, uglify().on('error', gutil.log))))
        .on('error', reject)
        .pipe(gulp.dest(bundleToPath))
        .on('end', resolve)
    @notify('Updated in '+(new Date().getTime() - startDate.getTime())+' ms')
    @notify 'Bundling scripts. SUCCESS.'

  bundleJs: (rootDir, entry, bundleToPath, bundleToName, appEnv)-> async =>
    @bundler = browserify({entries: [entry], debug: true, cache: {}, paths: [rootDir], extensions: ['.coffee', '.csx', '.css'], packageCache: {}})
    if @__HACKwatch
      @bundler.plugin(watchify, {ignoreWatch: false})
    @bundler.transform coffeeify,
      global: true
      bare: true
    @bundler.transform csxify,
      global: true
      bare: true
      mode: appEnv
      tagPrefixes: ['uma-', 'apphire-']

    @bundler.transform(browserifyCss,{autoInject: true, global: true, rootDir: rootDir})
    yield @rebundle(bundleToPath, bundleToName, appEnv)

  bundle: ()-> async =>
    @rootDir = @from.substr(0, @from.lastIndexOf("/"))

    @bundleToPath = @to.substr(0, @to.lastIndexOf("/"))
    @bundleToName = @to.substr(@to.lastIndexOf("/") + 1)
    yield @bundleJs(@rootDir, @from, @bundleToPath, @bundleToName, @appEnv)

  watch: ()->
    @notify "Watching scripts for bundle #{@bundleToName}"
    @bundler.on 'update', (files)=> async =>
      try
        @notify 'ReBundling scripts. Changed files are: '
        @notify files
        yield @rebundle(@bundleToPath, @bundleToName, @appEnv)
        @notify 'ReBundling scripts. SUCCESS.'
        @emit 'reload'
      catch e
        @error e
        @bundler.emit 'end'


