path = require('path')
gulp = require('gulp')
del = require('del')
Emitter = require('./emitter')

module.exports =
  class Assets extends Emitter
    cleanOutFolder = (to)-> async ->      
      yield del [ path.join to, '/**/*']
    
    copy = (from, to)-> async ->      
      return new Promise (resolve, reject)->
        gulp.src(path.join(from, '/**/*.*'))
          .pipe(gulp.dest(to))
          .on('error', reject)
          .on('end', resolve)

    constructor: ()->
      @notify = (msg)=>
        @emit 'notify', msg
      super() 
    
    bundle: (from, to, noFlush)-> async =>
      if not noFlush
        @notify 'Cleaning directory "' + to + '"'
        yield cleanOutFolder(to)
        @notify 'Cleaning directory "' + to + '". SUCCESS.'      
      @notify 'Copying Static files from ' + from + ' to ' + to + '.'
      yield copy(from, to)
      @notify 'Copying Static files from ' + from + ' to ' + to + '. SUCCESS.'
      

    watch: (from, to)->
      @notify 'Watching Static files of ' + from + '...'
      gulp.watch path.join(from, '/**/*.*'), (done)=> async =>
        @notify('Change detected in static files ' + from)        
        yield @bundle(from, to, true)
      return @

