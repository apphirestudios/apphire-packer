EventEmitter2 = require('eventemitter2').EventEmitter2
module.exports =
  class Emitter extends EventEmitter2
    notify: (msg)->
      @emit 'notify', msg
    error: (msg)->
      @emit 'error', msg  
    constructor: ()->
      super
        wildcard: true
        delimiter: ':'
