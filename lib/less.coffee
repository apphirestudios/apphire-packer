less = require('gulp-less')
sourcemaps = require 'gulp-sourcemaps'
gulp = require('gulp')
Emitter = require('./emitter')

module.exports =
  class Less extends Emitter
    constructor: ()->
      @notify = (msg)=>
        @emit 'notify', msg
      super()
    bundle: (from, to)-> async =>
      @notify 'Compiling Less from ' + from + '.'
      yield new Promise (resolve, reject)->
        gulp.src(from)
          .pipe(less())
          .on('error', reject)
          .pipe(gulp.dest(to))
          .on('end', resolve)
      @notify 'Compiling Less from ' + from + '. SUCCESS.'    

    watch: (from, to)->
      watchDir = from.substring(0, from.lastIndexOf('/'))
      @notify '[UMA-WATCHER]: Watching Less of ' + watchDir + '...'
      gulp.watch watchDir, (done)=> async =>
        @notify('Change detected in less files ' + from)        
        yield @bundle(from, to)
        @emit 'reload'