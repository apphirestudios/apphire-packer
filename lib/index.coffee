Assets = require './assets'
Stylus = require './stylus'
Less = require './less'
Script = require './scripts'
global.async = require 'co'
path = require 'path'
fs = require 'fs'
nodemon = require 'nodemon'

global.log = console.log.bind @

cwd = path.resolve('.') + '/'

process.on 'SIGINT', ->
  console.log 'Ctrl-C...'
  process.exit 2

global.app = {}


try
  configPath = process.argv[2] or './packer-config'
  app.config = require path.join cwd, (configPath)
catch e
  throw new Error 'Config file packer-config not found in your working directory \n' + e.stack or e

process.env.PORT ?= 5000
buildPath = path.join(cwd, './')

module.exports = class ApphirePacker
  pack: -> async =>
    command = process.argv[3] or 'pack'
    try
      switch command
        when 'pack'
          yield @buildFrontend(false)
        when 'develop'
          yield @buildFrontend(true)
          @watchFrontend()
          yield @startBackend()
        else
          throw new Error "Unknown command #{command}"
    catch e
      console.error e.stack or e

  #HACK: unfortunately I can't find the way to make watchify
  #working after first build done, the only way it works is
  #when enabled before bundling in buildScrips function
  buildFrontend:(__HACKwatch)-> async =>
    log 'Building Frontend components.'

    if app.config.assets?
      @assets = new Assets()
      @assets.on 'notify', (msg)=>
        log msg
      app.config.assets.from = path.join(cwd, app.config.assets.from)
      app.config.assets.to = path.join(buildPath, app.config.assets.to)
      yield @assets.bundle(app.config.assets.from, app.config.assets.to)

    if app.config.scripts?
      @scripts = {}
      for name, spec of app.config.scripts
        from = path.join(cwd, spec.from)
        to = path.join(buildPath, spec.to)
        @scripts[name] = new Script(from, to, process.env.NODE_ENV, __HACKwatch)
        @scripts[name].on 'notify', (msg)=>
          log msg
        @scripts[name].on 'error', (msg)=>
          log msg.stack or msg
        yield @scripts[name].bundle()

    if app.config.stylus?
      @stylus = new Stylus()
      @stylus.on 'notify', (msg)=>
        log msg
      app.config.stylus.from = path.join(cwd, app.config.stylus.from)
      app.config.stylus.to = path.join(buildPath, app.config.stylus.to)

      yield @stylus.bundle(app.config.stylus.from, app.config.stylus.to)

    if app.config.less?
      @less = new Less()
      @less.on 'notify', (msg)=>
        log msg
      app.config.less.from = path.join(cwd, app.config.less.from)
      app.config.less.to = path.join(buildPath, app.config.less.to)
      yield @less.bundle(app.config.less.from, app.config.less.to)

    log 'Frontend components has been succesfully built!'


  watchFrontend: ->
    if @assets?
      @assets.watch(app.config.assets.from, app.config.assets.to)
      @assets.on 'reload', => if @browserSync then @browserSync.reload()

    if @stylus?
      @stylus.watch(app.config.stylus.from, app.config.stylus.to)
      @stylus.on 'reload', => if @browserSync then @browserSync.reload()

    if @less?
      @less.watch(app.config.less.from, app.config.less.to)
      @less.on 'reload', => if @browserSync then @browserSync.reload()

    if @scripts?
      for name, script of @scripts
        script.watch()
        script.on 'reload', => if @browserSync then @browserSync.reload()

  startBackend: -> async =>
    @browserSync = require('browser-sync').create()
    @browserSync.init
      proxy: "http://localhost:#{process.env.PORT}"
      open: false
      ui: false
    log 'Starting backend components.'

    nodemonConfig =
      script: app.config.backendEntry
      args: process.argv.slice(2)
      exec: 'node --inspect'
      ext: 'js coffee json'
      watch: [path.join(cwd,'./packer-config.coffee')]

    if app.config.backendWatch then nodemonConfig.watch.push path.join(cwd, app.config.backendWatch, "/*")

    n = nodemon(nodemonConfig)
    n.on('start', ->
      log 'Backend watcher started'
    ).on('crash', (e)->
      log 'Error in backend components'
      #log e.stack or e
    ).on('quit', =>
      log 'Backend watcher exited'
      @browserSync.exit()
    ).on 'restart', (files) =>
      log 'Backend restarted due to changes: ', files

      if @browserSync then setTimeout @browserSync.reload, 5000