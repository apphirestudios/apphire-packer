stylus = require 'gulp-stylus'
sourcemaps = require 'gulp-sourcemaps'
gulp = require('gulp')
Emitter = require('./emitter')
path = require 'path'

module.exports =
  class Stylus extends Emitter
    constructor: ()->
      @notify = (msg)=>
        @emit 'notify', msg
      super()

    
    bundle: (from, to)-> async =>
      @notify 'Compiling Stylus from ' + from + '.'
      yield new Promise (resolve, reject)->
        gulp.src(from)
          .pipe(sourcemaps.init())
          .pipe(stylus())
          .on('error', reject)
          .pipe(sourcemaps.write('.'))
          .on('error', reject)
          .pipe(gulp.dest(to))
          .on('end', resolve)
      @notify 'Compiling Stylus from ' + from + '. SUCCESS.'    
    

    watch: (from, to)->
      watchDir = from.substring(0, from.lastIndexOf('/'))
      @notify 'Watching Stylus of ' + watchDir + '...'
      gulp.watch path.join(watchDir, '/**/*.styl'), (done)=> async =>
        @notify('Change detected in stylus files ' + from)        
        yield @bundle(from, to)
        @emit 'reload'
      return @  
