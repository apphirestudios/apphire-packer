through = require('through2')
coffee = require('coffee-script')
gutil = require('gulp-util')
applySourceMap = require('vinyl-sourcemaps-apply')
path = require('path')
merge = require('merge')
coffeescript = require('coffee-script')
csx = require('apphire-csx')
PluginError = gutil.PluginError

module.exports = (opt) ->

  replaceExtension = (path) ->
    path = path.replace(/\.coffee\.md$/, '.litcoffee')
    gutil.replaceExtension path, '.js'

  transform = (file, enc, cb) ->
    if file.isNull()
      return cb(null, file)
    if file.isStream()
      return cb(new PluginError('gulp-coffee', 'Streaming not supported'))
    data = undefined
    str = file.contents.toString('utf8')
    dest = replaceExtension(file.path)
    options = merge({
      bare: false
      header: false
      sourceMap: ! !file.sourceMap
      sourceRoot: false
      literate: /\.(litcoffee|coffee\.md)$/.test(file.path)
      filename: file.path
      sourceFiles: [ file.relative ]
      generatedFile: replaceExtension(file.relative)
    }, opt)
    try
      data = coffee.compile(csx.compile(str, options), options)
      #data = require('regenerator').compile(data).code
    catch err
      return cb(new PluginError('gulp-coffee', err))
    if data and data.v3SourceMap and file.sourceMap
      applySourceMap file, data.v3SourceMap
      file.contents = new Buffer(data.js)
    else
      file.contents = new Buffer(data)
    file.path = dest
    cb null, file
    return

  through.obj transform
