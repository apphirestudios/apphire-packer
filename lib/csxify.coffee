coffee = require('coffee-script')
convert = require('convert-source-map')
path = require('path')
through = require('through2')
filePattern = /\.csx$/i
csx = require 'apphire-csx'

isCsx = (file) ->
  filePattern.test file

ParseError = (error, src, file) ->

  ### Creates a ParseError from a CoffeeScript SyntaxError
     modeled after substack's syntax-error module
  ###

  SyntaxError.call this
  @message = error.message
  @line = error.location.first_line + 1
  # cs linenums are 0-indexed
  @column = error.location.first_column + 1
  # same with columns
  markerLen = 2
  if error.location.first_line == error.location.last_line
    markerLen += error.location.last_column - (error.location.first_column)
  @annotated = [
    file + ':' + @line
    src.split('\n')[@line - 1]
    Array(@column).join(' ') + Array(markerLen).join('^')
    'ParseError: ' + @message
  ].join('\n')
  return

compile = (filename, source, csxOptions, coffeeOptions, callback) ->
  `var error`
  compiled = undefined
  source = csx.compile(source, csxOptions)
  try
    compiled = coffee.compile source,
      sourceMap: coffeeOptions.sourceMap
      inline: true
      bare: coffeeOptions.bare
      header: coffeeOptions.header
  catch e
    error = e
    if e.location
      error = new ParseError(e, source, filename)
    callback error
    return
  if coffeeOptions.sourceMap
    map = convert.fromJSON(compiled.v3SourceMap)
    basename = path.basename(filename)
    map.setProperty 'file', basename.replace(filePattern, '.js')
    map.setProperty 'sources', [ basename ]
    callback null, compiled.js + '\n' + map.toComment() + '\n'
  else
    callback null, compiled + '\n'
  return

csxify = (filename, options) ->

  transform = (chunk, encoding, callback) ->
    chunks.push chunk
    callback()
    return

  flush = (callback) ->
    stream = this
    source = Buffer.concat(chunks).toString()
    compile filename, source, csxOptions, coffeeOptions, (error, result) ->
      if !error
        stream.push result
      callback error
      return
    return

  #log filename
  if !isCsx(filename)
    return through()
  if typeof options == 'undefined' or options == null
    options = {}

  csxOptions =
    mode: options?.mode or 'development'
    tagPrefixes: options?.tagPrefixes or []
    filename: filename

  coffeeOptions =
    sourceMap: options._flags and options._flags.debug
    bare: true
    header: false
  i = 0
  keys = Object.keys(coffeeOptions)
  while i < keys.length
    key = keys[i]
    option = options[key]
    if typeof option != 'undefined' and option != null
      if option == 'false' or option == 'no' or option == '0'
        option = false
      coffeeOptions[key] = ! !option
    i++


  chunks = []
  through transform, flush

ParseError.prototype = Object.create(SyntaxError.prototype)

ParseError::toString = ->
  @annotated

ParseError::inspect = ->
  @annotated

csxify.compile = compile
csxify.isCsx = isCsx
module.exports = csxify
