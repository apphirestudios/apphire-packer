module.exports =
  name: 'test-app'
  backendEntry: './index.js'
  scripts:
    from:  './app/app.coffee'
    to: './public/app.js'
  assets:
    from: './app/assets'
    to: './public/'
  stylus:
    from: './app/styles/style.styl'
    to: './public/'